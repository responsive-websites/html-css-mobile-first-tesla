// Menu Show
const navMenu = document.getElementById("nav-menu");
const toggleMenu = document.getElementById("nav-toggle");
const closeMenu = document.getElementById("nav-close");

// Show
toggleMenu.addEventListener("click", () => {
  navMenu.classList.toggle("show");
});

// Hide
closeMenu.addEventListener("click", () => {
  navMenu.classList.remove("show");
});
